package ca.cegepdrummond;

public class Serie2_Variables {
    /*
     * Complétez le code suivant afin de déclarer la
     * variable "varA" comme étant du type int
     * et ayant la valeur 1234
     */
    public void variable1() {
        /* enlever cette ligne de commentaire
        ??? varA ????
        System.out.println(varA);
        enlever cette ligne de commentaire */
    }

    /*
     * Assignez des valeurs aux variables a,b,c
     * a doit avoir la valeur 3
     * b doit avoir la valeur 4
     * c doit avoir la valeur 5
     *
     * Le println doit afficher  3 4 5
     */
    public void variable2() {
        /* enlever cette ligne de commentaire
        int a, b;
        int c;

        System.out.println(a + " " + b + " " + c );
        enlever cette ligne de commentaire */
    }

    /*
     * Ajoutez le nombre minimal de parenthèses () pour que ce code affiche 42
     */
    public void variable3() {
        /* enlever cette ligne de commentaire
        int a = 42 + 3 + 4 - 7 * 1 ; // ajoutez des parenthèses afin de regrouper les opérateurs mathématiques

        System.out.println( a );
        enlever cette ligne de commentaire */
    }

    /*
     * corrigez le code pour qu'il affiche le caractère 'a'
     */
    public void variable4() {
        /* enlever cette ligne de commentaire
        char x = "a"
        system.out.println(x);
        enlever cette ligne de commentaire */
    }



}
