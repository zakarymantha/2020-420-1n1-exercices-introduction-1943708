package ca.cegepdrummond;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Test_Serie5_OperateursBooleens extends SimulConsole {

    @Test
    @Order(1)
    void test_bool1() throws Exception {
        choixMenu("5a");
        assertSortie("bravo", false);

    }
    @Test
    @Order(2)
    void test_bool2() throws Exception {
        choixMenu("5b");
        assertSortie("bravo", false);

    }
    @Test
    @Order(3)
    void test_bool3() throws Exception {
        choixMenu("5c");
        assertSortie("bravo", false);

    }
    @Test
    @Order(4)
    void test_bool4() throws Exception {
        choixMenu("5d");
        assertSortie("bravo", false);

    }
    @Test
    @Order(5)
    void test_bool5() throws Exception {
        choixMenu("5e");
        assertSortie("bravo", false);

    }
    @Test
    @Order(6)
    void test_bool6() throws Exception {
        choixMenu("5f");
        assertSortie("bravo", false);

    }
    @Test
    @Order(7)
    void test_bool7() throws Exception {
        choixMenu("5g");
        assertSortie("bravo", false);

    }

    @Test
    @Order(8)
    void test_boolEtClavier1() throws Exception {
        choixMenu("5h");
        ecrire("0");
        assertSortie("OK", false);

        choixMenu("5h");
        ecrire("42");
        assertSortie("OK", false);

        choixMenu("5h");
        ecrire("-1");
        assertSortie("erreur", false);

        choixMenu("5h");
        ecrire("43");
        assertSortie("erreur", false);

        choixMenu("5h");
        ecrire("21");
        assertSortie("OK", false);

        choixMenu("5h");
        ecrire("64");
        assertSortie("erreur", false);
    }

    @Test
    @Order(9)
    void test_boolEtClavier2() throws Exception {
        choixMenu("5i");
        ecrire("0");
        assertSortie("OK", false);

        choixMenu("5i");
        ecrire("42");
        assertSortie("OK", false);

        choixMenu("5i");
        ecrire("-1");
        assertSortie("erreur", false);

        choixMenu("5i");
        ecrire("43");
        assertSortie("erreur", false);

        choixMenu("5i");
        ecrire("21");
        assertSortie("OK", false);

        choixMenu("5i");
        ecrire("64");
        assertSortie("erreur", false);
    }

    @Test
    @Order(10)
    void test_boolEtClavier3() throws Exception {
        choixMenu("5j");
        ecrire("0");
        assertSortie("OK", false);

        choixMenu("5j");
        ecrire("42");
        assertSortie("OK", false);

        choixMenu("5j");
        ecrire("-1");
        assertSortie("non", false);

        choixMenu("5j");
        ecrire("43");
        assertSortie("non", false);

        choixMenu("5j");
        ecrire("21");
        assertSortie("OK", false);

        choixMenu("5j");
        ecrire("4");
        assertSortie("non", false);
    }

    @Test
    @Order(11)
    void test_boolEtClavier4() throws Exception {
        choixMenu("5k");
        ecrire("0");
        assertSortie("6", false);

        choixMenu("5k");
        ecrire("42");
        assertSortie("6", false);

        choixMenu("5k");
        ecrire("-1");
        assertSortie("non", false);

        choixMenu("5k");
        ecrire("43");
        assertSortie("non", false);

        choixMenu("5k");
        ecrire("15");
        assertSortie("3", false);

        choixMenu("5k");
        ecrire("4");
        assertSortie("non", false);
    }

    @Test
    @Order(12)
    void test_boolEtClavier5() throws Exception {
        choixMenu("5l");
        ecrire("0");
        assertSortie("OK", false);

        choixMenu("5l");
        ecrire("12");
        assertSortie("OK", false);

        choixMenu("5l");
        ecrire("-1");
        assertSortie("non", false);

        choixMenu("5l");
        ecrire("-12");
        assertSortie("OK", false);

        choixMenu("5l");
        ecrire("30");
        assertSortie("non", false);

        choixMenu("5l");
        ecrire("4");
        assertSortie("non", false);
    }
}
